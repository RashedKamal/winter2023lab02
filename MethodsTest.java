public class MethodsTest{
	public static void main(String[] args){
		//String s1 = "java";
		//String s2 = "programming";
		//System.out.println(s1.length());
		//System.out.println(s2.length());
		//SecondClass.addOne(50);
		//SecondClass.addTwo(50);
		SecondClass second = new SecondClass();
		System.out.println(second.addTwo(50));
		
	}
	
	public static void methodNoInputNoReturn(){
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int input){
		input = input-5;
		System.out.println("Inside the method one input no return " + input);
}
	public static void methodTwoInputsNoReturn(int one, double two){
		System.out.println(one);
		System.out.println(two);
}

	public static int methodNoInputReturnInt(){
		return 5;
}

	public static double sumSquareRoot(int side1, int side2){
		int sum = side1+side2;
		double root = Math.sqrt(sum);
		return root;
}
}