import java.util.Scanner;
public class PartThree{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter 2 numerical values");
		double inputValue1 = reader.nextDouble();
		double inputValue2 = reader.nextDouble();
		
		System.out.println("The sum is: " + Calculator.addition(inputValue1, inputValue2));
		System.out.println("The difference is: " + Calculator.subtraction(inputValue1, inputValue2));
		
		Calculator calculator = new Calculator();
		System.out.println("The product is: " + calculator.multiplication(inputValue1, inputValue2));
		System.out.println("The quotient is: " + calculator.division(inputValue1, inputValue2));
	}
}